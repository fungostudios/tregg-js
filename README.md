# TreggSDK

TreggSDK is distributed with 3 files:
`config.min.js`
`vendor.min.js`
`app.min.js`

`config.min.js` contains some configuration that depends on each app such as `baseApiUrl` and `client_id`.

`app.min.js` is the sdk itself and contains some methods to communicate with tregg store server.

`vendor.min.js` is used to import all the dependencies required by the sdk which are:

  - jquery
  - handlebars
  - handlebars-runtime
  - ember
  - ember-data
  - ember-auth (custom) with:
    + ember-auth-request-jquery
    + ember-auth-response-json
    + ember-auth-session-local-storage
    + ember-auth-strategy-token.js
    + ember-auth-module-ember-data.js
    + ember-auth-module-rememberable.js
  - From ember-app-kit
    + loader
    + Resolver

If you already use one or more of those libraries you should not import the vendor.min.js in your index.html but you have to manually import all the libraries (or the ones you missed)
Be sure to import files in the following order:

  1. `config.min.js`
  2. `vendor.min.js`
  3. `app.min.js`

TreggSdk namespace is `Tregg`.
All methods exposed that interacts with the server need to be called as authenticated user. 
So when application starts you must call:

`Tregg.clientInitialize('client_id_given')` this method does not return a promise and it's used to set the clien_it of your application

After that, in order to perform requests to the server, the user must be authenticated

`Tregg.signIn('test@gmail.com', 'password')`

(Method for `Tregg.signUp` and `Tregg.signOut` are also available)


All the following methods require to be authenticated and all return a promise 

`Tregg.getUserId()` -> promise (once resolved returns the current logged in user id)

Example:

```
var user_id;
Tregg.getUserId().then(function(res) {
 user_id = res;
 console.log('user id found!');
}, function(error) {
  console.log('userId not found');
});

user_id  // e.g 56

```
`Tregg.getItems()` -> promise (once resolved returns an array of items)

`Tregg.getDeals()` -> promise (once resolved returns an array of deals) 

`Tregg.addEmptyInventoryToUser()` -> promise (once resolved returns the id of the newly created inventory)

`Tregg.listAllInventories()` -> promise (once resolved returns an array of inventory objects)

`Tregg.updateInventory(inventory_id, itemsJSON)` -> promise (once resolved returns the inventory just updated)

Item_deals and gateway_deals objects have a purchase method which can be invoked directly from the object

E.g

```
var deals;
Tregg.getDeals().then(function(res){
  deals = res;
});
item_deal = deals.objectAt(4).get('item_deals').objectAt(0)
item_deal.purchase(12) //12 is the inventory_id in which put the item acquired through a deals and from which get the items for paying
```
 
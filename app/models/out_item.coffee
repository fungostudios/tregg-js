`import Deal from 'appkit/models/deal';`
`import Item from 'appkit/models/item';`

OutItem = DS.Model.extend(
  qty: DS.attr("number")
  item: DS.belongsTo('item')
  deal: DS.belongsTo('deal')
)

OutItem.FIXTURES = [
  id: 1
  qty: 100
  item: 1
  deal: 1
,
  id: 2
  qty: 200
  item: 2
  deal: 2
,
  id: 3
  qty: 300
  item: 3
  deal: 3
,
  id: 4
  qty: 300
  item: 4
  deal: 4
]

`export default OutItem;`

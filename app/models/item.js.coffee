Item = DS.Model.extend(
  name: DS.attr("string")
  description: DS.attr("string")
  image_url: DS.attr("string")
  tags: DS.attr("string")
)

Item.FIXTURES = [
  id: 1
  name: "item 1"
  description: "description 1"
  tags: "tag 1, tag 1.1"
  image_url: ""
,
  id: 2
  name: "item 2"
  description: "description 2"
  tags: "tag 2, tag 2.1"
  image_url: ""
,
  id: 3
  name: "item 3"
  description: "description 3"
  tags: "tag 3, tag 3.1"
  image_url: ""
,
  id: 4
  name: "item 4"
  description: "description 4"
  tags: "tag 4, tag 4.1"
  image_url: ""
,
  id: 5
  name: "item 5"
  description: "description 5"
  tags: "tag 5, tag 5.1"
  image_url: ""
]

`export default Item;`
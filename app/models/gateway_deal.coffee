`import Deal from 'appkit/models/deal';`
`import Gateway from 'appkit/models/gateway';`

GatewayDeal = DS.Model.extend(
  product_id: DS.attr("string")
  gateway: DS.belongsTo('gateway')
  deal: DS.belongsTo('deal')

  purchase: (receipt, transaction_id, product_id, inventory_id) ->
    url = window.ENV.baseApiUrl + this.get('gateway').get('name') + '/purchases';
    header = 'OAuth ' + localStorage.getItem('access_token');
    data = 
      receipt: receipt,
      transaction_id: transaction_id ,
      product_id: product_id,
      inventory_id: inventory_id,
      sandbox: FALSE

    new Ember.RSVP.Promise((resolve, reject) ->
      Ember.$.ajax url,
        data: data
        type: "POST"
        headers:
          Authorization: header

        success: (res) ->
          resolve res

        error: (errors) ->
          reject errors
      return
    )
)

GatewayDeal.FIXTURES = [
  id: 1
  product_id: "id1"
  gateway: 1
  deal: 1
,
  id: 2
  product_id: "id2"
  gateway: 2
  deal: 2
,
  id: 3
  product_id: "id3"
  gateway: 3
  deal: 3
]

`export default GatewayDeal;`
`import Item from 'appkit/models/item';`
`import ItemDeal from 'appkit/models/item_deal';`

InItem = DS.Model.extend(
  qty: DS.attr("number")

  item: DS.belongsTo('item')
  item_deal: DS.belongsTo('item_deal')
)

InItem.FIXTURES = [
  id: 1
  qty: 100
  item: 1
  item_deal: 1
,
  id: 2
  qty: 2
  item: 2
  item_deal: 1
,
  id: 3
  qty: 3
  item: 3
  item_deal: 2
,
  id: 4
  qty: 4
  item: 4
  item_deal: 2
]

`export default InItem;`
`import GatewayDeal from 'appkit/models/gateway_deal';`

Gateway = DS.Model.extend(
  name: DS.attr("string")
  public_key: DS.attr("string")
  provider_url: DS.attr("string")
  
  gateway_deals: DS.hasMany('gateway_deal')
)

Gateway.FIXTURES = [
  id: 1
  name: "apple"
  public_key: "public_key 1"
  provider_url: "http: 888"
  gateway_deals: [1]
,
  id: 2
  name: "windows"
  public_key: "public_key 2"
  provider_url: "http: 555"
  gateway_deals: [2]
,
  id: 3
  name: "android"
  public_key: "public_key 3"
  provider_url: "http: 444"
  gateway_deals: [3]
]

`export default Gateway;`

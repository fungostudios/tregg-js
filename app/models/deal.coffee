`import GatewayDeal from 'appkit/models/gateway_deal';`
`import ItemDeal from 'appkit/models/item_deal';`
`import OutItem from 'appkit/models/out_item';`

Deal = DS.Model.extend(
  name: DS.attr("string")
  description: DS.attr("string")
  image_url: DS.attr("string")
  tags: DS.attr("string")
  onSale: DS.attr("number")
  start_date: DS.attr("number")
  finish_date: DS.attr("number")

  item_deals: DS.hasMany('item_deal')
  gateway_deals: DS.hasMany('gateway_deal')
  out_items: DS.hasMany('out_item')
)

Deal.FIXTURES = [
  id: 1
  name: "deal 1"
  description: "prova descr 1"
  image_url: "http://"
  tags: "tag 1, tag 1.1"
  onSale: 1
  start_date: 1382631006
  finish_date: 1382631006
  gateway_deals: [1]
  out_items: [1, 2]
  item_deals: [1]
,
  id: 2
  name: "deal 2"
  description: "prova descr 2"
  image_url: "http://"
  tags: "tag 2, tag 2.1"
  onSale: 1
  start_date: 1382631006
  finish_date: 1382631006
  gateway_deals: [2]
  out_items: [3, 4]
  item_deals: [2]
,
  id: 3
  name: "deal 3"
  description: "prova descr 3"
  image_url: "http://"
  tags: "tag 3, tag 3.1"
  onSale: 1
  start_date: 1382631006
  finish_date: 1382631006
  gateway_deals: [3]
  out_items: [1, 2]
  item_deals: [1]
,
  id: 4
  name: "deal 4"
  description: "prova descr 4"
  image_url: "http://"
  tags: "tag 4, tag 4.1"
  onSale: 1
  start_date: 1382631006
  finish_date: 1382631006
  gateway_deals: [1]
  out_items: [1, 2]
  item_deals: [1]
]

`export default Deal;`

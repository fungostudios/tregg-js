`import InItem from 'appkit/models/in_item';`
`import Deal from 'appkit/models/deal';`

ItemDeal = DS.Model.extend(
  in_items: DS.hasMany('in_item')
  deal: DS.belongsTo('deal')

  purchase: (inventory_id) ->
    url = window.ENV.baseApiUrl + '/virtual/purchases';
    header = 'OAuth ' + localStorage.getItem('access_token');
    data = 
      item_deal_id: this.get('id'),
      inventory_id: inventory_id,
      source_inventory_id: inventory_id

    new Ember.RSVP.Promise((resolve, reject) ->
      Ember.$.ajax url,
        data: data
        type: "POST"
        headers:
          Authorization: header

        success: (res) ->
          resolve res

        error: (errors) ->
          reject errors
      return
    )
)

ItemDeal.FIXTURES = [
  id: 1
  in_items: [1,2]
  deal: 1
,
  id: 2
  in_items: [3,4]
  deal: 2 
]

`export default ItemDeal;`

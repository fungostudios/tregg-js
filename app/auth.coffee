Auth = Ember.Auth.extend
  tokenLocation: "authHeader"
  tokenKey: "access_token"
  baseUrl: window.ENV.baseApiUrl
  tokenHeaderKey: 'OAuth'
  modules: ["ember_data", "remeberable"]

`export default Auth;`
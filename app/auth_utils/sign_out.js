export default function signOut() {
  var accessToken = localStorage.getItem("access_token");
  var header = 'OAuth ' + this.auth.get('authToken');
  var url = window.ENV.baseApiUrl + '/sign-out';
  localStorage.removeItem("access_token");
  localStorage.removeItem('user_id');
  localStorage.removeItem('inventory_id');

  //signOut without hitting the server
  this.auth.destroySession(JSON.stringify({
    access_token: accessToken
  }));

  Ember.$.ajax(url, {
    type: "POST",
    headers: {"Authorization": header},
    success: function() {
      return console.log('successfull logout');
    },
    error: function() {
      return console.log('error on logOut');
    }
  });
}
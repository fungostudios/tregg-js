export default function validateEmail (email) {
  var emailRegExp;
  emailRegExp = new RegExp(/^((?!\.)[a-z0-9._%+-]+(?!\.)\w)@[a-z0-9-\.]+\.[a-z.]{1,5}(?!\.)\w$/i);
  return emailRegExp.test(email);
}
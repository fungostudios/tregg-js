export default function signUp(username, password) {
  if (this.validateEmail(username)) {
    if ((password !== null) && (username !== null)) {

      var clientId = window.ENV.client_id;
      var signUpUrl = window.ENV.baseApiUrl + "/sign-up";
      var data = {
        username: username,
        password: password,
        client_id: clientId
      };
      return Ember.$.post(signUpUrl, data, (function(_this) {
        return function(response) {
            var accessToken;
            accessToken = response.access_token;
            if (accessToken) {
              _this.auth.createSession(JSON.stringify({
                access_token: accessToken
              }));
            return localStorage.setItem("access_token", accessToken);
          }
        };
      })(this)).fail((function(_this) {
        return function(jqxhr, textStatus, error) {
          var errs;
          errs = JSON.parse(jqxhr.responseText);
          return _this.set("error", errs.error);
        };
      })(this));
    } else {
      return this.set("error", "All fields must be filled");
    }
  } else {
    return this.set("error", "Email should be in a valid format");
  }
}
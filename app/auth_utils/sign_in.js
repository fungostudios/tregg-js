import getUserId from 'appkit/sdk/get_user_id';
export default function signIn(username, password) {
  if ((password !== null) && (username !== null)) {
    if (this.validateEmail(username)) {
      var _this = this;
      var clientId = window.ENV.client_id;
      var data = {
          client_id: clientId,
          username: username,
          password: password,
          grant_type: "password"
      };

      this.auth.signIn({
        data: data
      });

      this.auth.addHandler('signInSuccess', (function(_this) {
        return function(response) {
          var accessToken = _this.auth.get('authToken');
          getUserId.apply(_this).then(function(id) {
            localStorage.setItem('user_id', id);
          });
          if (accessToken) {
            console.log("success signIn");
            localStorage.setItem("access_token", accessToken);
          }
        };
      })(this));

      this.auth.addHandler('signInError', function(error) {
        console.log("fail signIn");
      });

    } else {
      return this.set("error", "Email should be in a valid format");
    }
  } else {
    return this.set("error", "All fields must be filled");
  }
}
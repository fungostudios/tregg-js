//items array shouòd be a Json string or a Json Object
export default function updateInventory(inventoryId, itemsJSON) {
  var url = window.ENV.baseApiUrl + '/inventories/' + inventoryId + '/items';
  var header = 'OAuth ' + this.auth.get('authToken');
  return new Ember.RSVP.Promise(function(resolve, reject){
    Ember.$.ajax(url,{
      headers: {"Authorization": header},
      type: "POST",
      data: {items: itemsJSON},
      success: function(res) {
        return resolve(res);
      },
      error: function(errors) {
        return reject(errors);
      }
    });
  });
}
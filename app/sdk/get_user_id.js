//the function returns a promise that once resolved returns the current user id
export default function getUserId() {
  var url = window.ENV.baseApiUrl + '/me';
  var header = 'OAuth ' + this.auth.get('authToken');
  return new Ember.RSVP.Promise(function(resolve, reject){
    Ember.$.ajax(url, {
      headers: {"Authorization": header},
      success: function(res) {
        return resolve(res.user.id);
      },
      error: function(errors) {
        return reject(errors);
      }
    });
  }); 
}
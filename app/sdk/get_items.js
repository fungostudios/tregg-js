// this return a promise, in order to have an array of items you should
// do something like 
//
// var items;
// TREGG.getItems().then(function(res) { items = res.content})
export default function getItems() {
  return this.store.find('item');
}
//the function returns the id of the just created inventory
export default function addEmptyInventoryToUser() {
  var url = window.ENV.baseApiUrl + '/users/' + localStorage.getItem('user_id') + '/inventories';
  var header = 'OAuth ' + this.auth.get('authToken');
  return new Ember.RSVP.Promise(function(resolve, reject){
    Ember.$.ajax(url, {
      headers: {"Authorization": header},
      type: "POST",
      success: function(res) {
        return resolve(res.inventory.id);
      },
      error: function(errors) {
        return reject(errors);
      }
    });
  });
}
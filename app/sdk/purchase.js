//the function returns a promise that once resolved returns all the inventory associated to a user
export default function purchase(item_deal_id, inventory_id) {
  var url = window.ENV.baseApiUrl + '/virtual/purchases';
  var header = 'OAuth ' + this.auth.get('authToken');
  var data = {
    item_deal_id: item_deal_id,
    inventory_id: inventory_id,
    source_inventory_id: inventory_id
  };
  return new Ember.RSVP.Promise(function(resolve, reject){
    Ember.$.ajax(url,{
      data: data,
      type: "POST",
      headers: {"Authorization": header},
      success: function(res) {
        return resolve(res);
      },
      error: function(errors) {
        return reject(errors);
      }
    });
  });
}
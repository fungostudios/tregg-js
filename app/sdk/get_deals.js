//to access to a single deal or to relationships items you should do something like
// deal = deals.objectAt(4) -> get a single deal
// gateway_deals = deal.get('gateway_deals') -> get all the gateway_deals
// gateway_deal = gateway_deals.objectAt(0) -> get single specified gateway_deal
export default function getDeals() {
  return this.store.find('deal');
}
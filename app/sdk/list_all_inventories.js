//the function returns a promise and when it is resolved it returns all the inventories for a user
export default function listAllInventories() {
  var url = window.ENV.baseApiUrl + '/users/' + localStorage.getItem('user_id') + '/inventories';
  var header = 'OAuth ' + this.auth.get('authToken');  
  return new Ember.RSVP.Promise(function(resolve, reject){
    Ember.$.ajax(url, {
      headers: {"Authorization": header},
      success: function(res) {
        return resolve(res);
      },
      error: function(errors) {
        return reject(errors);
      }
    });
  });
}
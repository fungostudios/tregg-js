ApplicationSerializer = DS.ActiveModelSerializer.extend
  serializeAttribute: (record, json, key, attribute) ->
    attrs = Ember.get(this, "attrs")
    value = Ember.get(record, key)
    type = attribute.type
    if type
      transform = @transformFor(type)
      value = transform.serialize(value)
    # if provided, use the mapping provided by `attrs` in
    # the serializer
    key = attrs and attrs[key] or Ember.String.decamelize(key)
    json[key] = value
  typeForRoot: (root) ->
    Ember.String.singularize(root)

`export default ApplicationSerializer;`
ApplicationAdapter = DS.RESTAdapter.extend
  host: window.ENV.baseApiUrl,
  pathForType: (type) ->
    underscored = Ember.String.underscore(type)
    return Ember.String.pluralize(underscored)

`export default ApplicationAdapter;`
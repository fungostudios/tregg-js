import Resolver from 'resolver';
import Auth from 'appkit/auth';
import Store from 'appkit/store';

//auth method
import signIn from 'appkit/auth_utils/sign_in';
import signUp from 'appkit/auth_utils/sign_up';
import signOut from 'appkit/auth_utils/sign_out';
import validateEmail from 'appkit/auth_utils/validate_email';
import clientInitialize from 'appkit/auth_utils/client_initialize';

//TreggStore methods
import getDeals from 'appkit/sdk/get_deals';
import getItems from 'appkit/sdk/get_items';
import getUserId from 'appkit/sdk/get_user_id';
import addEmptyInventoryToUser from 'appkit/sdk/add_empty_inventory_to_user';
import listAllInventories from 'appkit/sdk/list_all_inventories';
import purchase from 'appkit/sdk/purchase';
import updateInventory from 'appkit/sdk/update_inventory';

var App = Ember.Application.create({
  //g73g5pdr1zi9j6u81percc3rwr7qq12
  LOG_ACTIVE_GENERATION: true,
  LOG_MODULE_RESOLVER: true,
  LOG_TRANSITIONS: true,
  LOG_TRANSITIONS_INTERNAL: true,
  LOG_VIEW_LOOKUPS: true,
  modulePrefix: 'appkit',
  Resolver: Resolver['default'],
  Store: Store,
  Auth: Auth,

  //auth methods
  clientInitialize: clientInitialize,
  signIn: signIn,
  signUp: signUp,
  signOut: signOut,
  validateEmail: validateEmail,

  // public methods
  // getUserId() -> promise (once resolved returns the current logged in user id)
  getUserId: getUserId,
  //getItems() -> promise (once resolved returns an array of items)
  getItems: getItems,
  //getDeals() -> promise (once resolved returns an array of deals) 
  getDeals: getDeals,
  //addEmptyInventoryToUser() -> promise (once resolved returns the id of the newly created inventory)
  addEmptyInventoryToUser: addEmptyInventoryToUser, 
  //listAllInventories() -> promise (once resolved returns an array of inventory objects)
  listAllInventories: listAllInventories, 
  //updateInventory(inventory_id, itemsArrayJSON) -> promise
  updateInventory: updateInventory,
  //purchase(item_deal_id, inventory_id) -> promise (once resolved returns )
  purchase: purchase,

  //function called after all initializers are resolved
  ready: function() {
    this.store = App.__container__.lookup('store:main');
    this.auth = App.__container__.lookup('auth:main');
    var accessToken = localStorage.getItem("access_token");
    if (accessToken) {
      console.log("fetched the accessToken");
      this.auth.createSession(JSON.stringify({
        access_token: accessToken
      }));
    } 
  }

});

export default App;

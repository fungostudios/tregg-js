// Put your test configuration here.
//
// This is useful when using a separate API
// endpoint in test than in production.
//
// window.ENV.public_key = '123456'
window.ENV = {
  baseApiUrl: "http://willy-pc.local:9000", //TODO: should be related to aws
  title: 'TreggEditor',
  client_id: "xxxxxxxxx"
};
